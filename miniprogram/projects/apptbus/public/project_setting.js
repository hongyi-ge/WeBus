module.exports = { //apptbus
	PROJECT_COLOR: '#C69C6D',
	NAV_COLOR: '#ffffff',
	NAV_BG: '#C69C6D',

	// setup
	SETUP_CONTENT_ITEMS: [
		{ title: '关于我们', key: 'SETUP_CONTENT_ABOUT' },
	],

	// 用户
	USER_REG_CHECK: false,
	USER_FIELDS: [
	],

	NEWS_NAME: '通知公告',
	NEWS_CATE: [
		{ id: 1, title: '通知公告', style: 'leftpic' },
	],
	NEWS_FIELDS: [

	],

	MEET_NAME: '车辆预约',
	MEET_CATE: [
		{ id: 1, title: '中巴车', style: 'leftbig1' },
		{ id: 2, title: '大巴车', style: 'leftbig1' },
	],
	MEET_CAN_NULL_TIME: false, // 是否允许有无时段的日期保存和展示
	MEET_FIELDS: [
		{ mark: 'cover', title: '封面图片', type: 'image', min: 1, max: 1, must: true },
		{ mark: 'desc', title: '起点-终点', type: 'textarea', max: 100, must: true },
		{ mark: 'content', title: '详情', type: 'content', must: true },
		{ mark: 'price', title: '价格(元)', type: 'digit', must: true },
		{
			mark: 'line', title: '站点', type: 'rows',
			ext: {
				titleName: '站点',
				hasDetail: false,
				hasVal: false,
				maxCnt: 30,
				minCnt: 1,
				checkDetail: false,
				hasPic: false,
				checkPic: false,
				titleMode: 'input'
			},
			must: true
		},
	],

	MEET_JOIN_FIELDS: [
		{ mark: 'name', type: 'text', title: '姓名', must: true, min: 2, max: 30, edit: false },
		{ mark: 'phone', type: 'text', len: 11, title: '手机号', must: true, edit: false },
	],

	// 时间默认设置
	MEET_NEW_NODE:
	{
		mark: 'mark-no', start: '07:00', end: '07:30', limit: 30, isLimit: true, status: 1,
		stat: { succCnt: 0, cancelCnt: 0, adminCancelCnt: 0, }
	},
	MEET_NEW_NODE_DAY: [
		{
			mark: 'mark-am', start: '07:00', end: '07:30', limit: 30, isLimit: true, status: 1,
			stat: { succCnt: 0, cancelCnt: 0, adminCancelCnt: 0, }
		},
		{
			mark: 'mark-pm', start: '12:00', end: '12:30', limit: 30, isLimit: true, status: 1,
			stat: { succCnt: 0, cancelCnt: 0, adminCancelCnt: 0, }
		}
		,
		{
			mark: 'mark-pm', start: '18:00', end: '18:30', limit: 30, isLimit: true, status: 1,
			stat: { succCnt: 0, cancelCnt: 0, adminCancelCnt: 0, }
		}
	],


}